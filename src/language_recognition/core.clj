(ns language-recognition.core)


(def portuguese {:characters        [\ã \õ \â \ê \ô \á \é \í \ó \ú \à \ç]
                 :one-letter-word   [\a \à \e \é \o]
                 :two-letter-word   ["ao" "as" "às" "da" "de" "do" "em", "os" "ou" "um"]
                 :three-letter-word ["aos" "com" "das" "dos" "ele" "ela" "mas" "não" "por" "que" "são" "uma"]})

(def english {:words            "a", "an", "and", "in", "of", "on", "the", "that", "to", "is", "I" ;should always be a capital
              :letter-sequences "th" "ch" "sh" "ough" "augh"
              :word-endings     "ing", "tion", "ed", "age", "s", "’s", "’ve", "n’t", "’d"})


(def accents-regex #"[\u00C0-\u00FF]")


(defn space? [letter]
  (let [space-regex #"\s"]
    (re-find space-regex (str letter))))


(defn punctuation? [character]
  (let [punctuation-regex #"[.,\/#!$%\^&\*;:{}=\-_`~()?]"]
    (re-find punctuation-regex (str character))))


(defn detection
  ([phrase] (->> (seq phrase)
                 (remove #(or (space? %) (punctuation? %)))
                 (frequencies)))
  ([phrase n-gram] (->> (seq phrase)
                        (remove #(or (space? %) (punctuation? %)))
                        (partition n-gram 1)
                        (map #(clojure.string/join "" %))
                        (frequencies))))


(defn bigrams [phrase] (detection phrase 2))


(defn detection-percentage [phrase]
  (let [grams (detection phrase)
        size (count grams)
        change-percent-fn (fn [acc [k v]]
                            (let [val (/ (* v 100.0) size)]
                              (assoc acc k (format "%.2f" val))))]
    (reduce change-percent-fn {} grams)))