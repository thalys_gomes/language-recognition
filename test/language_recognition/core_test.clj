(ns language-recognition.core-test
  (:require [clojure.test :refer :all]
            [language-recognition.core :refer :all]))


(deftest detection-english-test
  (testing "letter by letter"
    (is (= {\a 2, \e 2, \g 1, \h 3, \i 4, \l 1, \n 2, \p 1, \r 1, \s 4, \t 1}
           (detection "this is a phrase in english")))))


(deftest detection-portuguese-test
  (testing "portuguese phrase"
    (is (= {\a 2, \á 1, \e 3, \f 1, \g 1, \é 1, \ê 1, \m 2, \o 1, \p 1, \r 2, \s 3, \t 2, \u 3}
           (detection "está é uma frase em português.")))))


(deftest space?-test
  (is (space? " "))
  (testing "others characters"
    (is (not (space? \a)))))


(deftest punctuation?-test
  (is (= "!" (punctuation? \!))))


(deftest bigrams-test
  (is (= {"en" 1, "is" 3, "si" 1, "hr" 1, "sa" 1, "ph" 1, "hi" 1, "hp" 1, "se" 1,
          "ng" 1, "gl" 1, "ae" 1, "ra" 1, "sh" 1, "th" 1, "as" 1, "li" 1}
         (bigrams "this is a english phrase"))))


(deftest detection-percentage-test
  (is (= {\p "12.50", \e "37.50", \r "12.50", \c "12.50",
          \n "12.50", \t "12.50", \a "12.50", \g "12.50"}
         (detection-percentage "percentage"))))
