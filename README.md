# language-recognition

Description of the problem:

One approach to language detection is based on statistics about the characters appearing in the text. The most basic version of that is simply how often each single letter appears, but this idea can be extended to look at sequences of 2 or more letters. The general form of this is sometimes called n-grams.

So what I'd like you to put together is a function that will receive some input text and compute the frequency of occurrence of each letter.

Extensions if time allows:

- Filter out whitespace and punctuation
- Refactor for the code to be more functional (let them figure out what's the best they can do about it but the goal is to treat input as character stream, reduce it to get map of character counts, map to get frequencies)
- Extend the scope to 2-grams. This is pretty tricky to do purely functional - let them roll back to imperative if they struggle too long. Make sure 2-grams aren't considered for adjacent words.
- Generalise to n-grams
- Change the statistics to give a percentage of the characters instead of absolute count


## Usage

FIXME

## License

Copyright © 2018 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.

